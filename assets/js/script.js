class Pokemon {
	constructor(name, level, enemy) {
		this.name = name;
		this.level = level;
		this.enemy = enemy;
		this.health = 100 + level * 2;
		this.attack = this.level * 1.25 + 20;
		this.tackle = function() {
			this.enemy.health -= this.attack;
			if (this.enemy.health < 0) {
				this.enemy.die();
			} else {
				console.log(`${this.name} attacked ${this.enemy.name} with ${this.attack} damage`);
				console.log(`${this.enemy.name} health is now ${this.enemy.health}`);
			}
		};
		this.die = function() {
			this.health = 0;
			console.log(`${this.name} is dead`);
		};
	}
}

let Geodude;
let Pikachu = new Pokemon('Pikachu', 12, Geodude);

Geodude = new Pokemon('Geodude', 12, Pikachu);
Pikachu.enemy = Geodude;
